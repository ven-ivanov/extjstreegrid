/**
 * Created by Ven Ivanov on 2/5/15.
 */
Ext.define('Dag.model.Treegrid', {
    extend: 'Ext.data.Model',
    fields: [
        {name: 'HeaderText', type: 'string', mapping:'header'},
        {name: 'modelType', type: 'string', mapping:'modelType'},
        {name: 'ProductNo', type:'string', mapping:'productNo'},
        {name : 'PricePerItem', type:'float', mapping:'pricePerItem'},
        {name : 'vatCode', type:'float', mapping:'vatCode'},
        {name : 'Flag', type:'string', mapping:'flag'},
      /*  {name: 'Expanded',type:'boolean', maping :'expanded',convert:function(newValue,model){
            return true;
        }},*/
        {name : 'Items', type:'integer',mapping:'items',convert:function(newValue,model)
        {
          //  console.log('newValue:'+newValue);
            var modelType = model.get('modelType');
            if(typeof modelType == 'undefined' || modelType =='')
                return;

         //   console.log(modelType);
           // console.log(model.get('HeaderText'));

            if(modelType =='invoice')
            {
                return newValue;
            }
            else if(modelType =='chunk'){

                var children =  model.get('children');
                return children? children.length :0;
            }else if(modelType =='section')
            {
                var children =  model.get('children');
                if(!children)
                    return 0;
                //in case of scetion
                var total = 0;
                for(var i=0;i<children.length; i++)
                {
                    if(! children[i].children.length) continue;
                    for(var j=0;j<children[i].children.length;j++)
                        total += children[i].children[j].items;
                }


                return total;
            }
        }},
        {name : 'Sum',type:'float', convert: function(newValue,model){
           // console.log('Sum');
            var modelType = model.get('modelType');
            if(typeof modelType == 'undefined' || modelType =='')
                return;
                //modelType = 'invoice';
           // console.log(modelType);
            if(modelType =='invoice')
            {
                return model.get('Items') * model.get('PricePerItem');
            }
            else if(modelType =='chunk'){
                console.log('chunk updated');
                var children =  model.get('children');
                var sum = 0;

                for(var i =0; i< children.length; i++)
                    sum += children[i].items * children[i].pricePerItem;
                return sum;
            }else if(modelType =='section')
            {
                var children =  model.get('children');
                var sum = 0;

                for(var i =0; i< children.length; i++) {
                    if(! children[i].children.length) continue;
                    for(var j=0;j<children[i].children.length;j++)
                        sum += children[i].children[j].items * children[i].children[j].pricePerItem;
                }
                return sum;
            }
        }},
        {name : 'Vat',type:'float', convert: function(newValue,model){
          //  console.log('Vat');
            var modelType = model.get('modelType');
            if(typeof modelType == 'undefined' || modelType =='')
                return;
                //modelType = 'invoice';

            if(modelType =='invoice')
            {
                return model.get('Items') * model.get('PricePerItem') * model.get('vatCode') / 100;
            }
            else if(modelType =='chunk'){
                var children =  model.get('children');
                var vat = 0;

                for(var i =0; i< children.length; i++)
                    vat += children[i].items * children[i].pricePerItem * children[i].vatCode /100;
                return vat;
            }else if(modelType =='section')
            {
                var children =  model.get('children');
                var vat = 0;

                console.log('section VAT' );
                console.log(children);
                for(var i =0; i< children.length; i++)
                {
                     if(! children[i].children.length) continue;
                    for(var j =0; j< children[i].children.length; j++)
                            vat += children[i].items * children[i].children[j].pricePerItem * children[i].children[j].vatCode /100;
                }

                return vat;
            }
        }}
    ],
    set: function(fieldName,value) {

        this.callParent(arguments);
        if(fieldName==='Items' || fieldName ==='PricePerItem')
        {
            this.set('Sum');
            this.set('Vat');

        }
        if(fieldName ==='vatCode' && this.modelType !='invoice')
        {
            this.set('Vat');

        }

    },
   listeners :{
       idchanged:function(me,oldid, newid)
        {
            console.log('id changed');
        }
   }
});
