Ext.define('Dag.view.Main', {
    extend: 'Ext.container.Container',
    requires:[
        'Ext.tab.Panel',
        'Ext.layout.container.Border',
        'Dag.view.TreeGrid'
    ],
    xtype: 'app-main',

    layout: {
        type: 'border'
    },

    items: [{
        region: 'west',
        xtype: 'panel',
        title: 'Possible Menu',
        width: 150
    },{
        region: 'center',
        xtype: 'tree-grid',
        items:[{
            title: 'Tree Grid for Dag'
        }]
    }]
});