/**
 * Created by Ven Ivanov on 2/5/15.
 *
 */
Ext.define('Dag.view.TreeGrid', {
    extend: 'Ext.tree.Panel',

    requires: [
        'Ext.data.*',
        'Ext.grid.*',
        'Ext.tree.*',
        'Ext.ux.CheckColumn',
        'Dag.model.Treegrid'
    ],
    xtype: 'tree-grid',
    region: 'center',
    reserveScrollbar: true,
    title: 'Custom Tree Grid',
    height: 300,
    useArrows: true,
    rootVisible: false,
    multiSelect: true,
    singleExpand: true,
    viewConfig : { toggleOnDblClick :false},
    initComponent: function() {
        this.width = 600;

        // the renderer. You should define it within a namespace
        var comboBoxRendererProdcutNo = function(combo) {
            return function(value) {
                if(value == null || value =='' || typeof value ==='undefined')
                    return '';
                var idx = combo.store.find(combo.valueField, value);
                var rec = combo.store.getAt(idx);
                return (rec === null ? '' : rec.get(combo.displayField) );
            };
        }
        // the combo store
        var store_prodcutno = new Ext.data.SimpleStore({
            fields: [ "value","text" ],
            data: [
                [1, "Pro #1" ],
                [2,"Pro #2"] ,
                [3, "Pro #3" ],
                [4,"Pro #4" ],
                [5, "New #5" ],
                [6,"New #6"] ,
                [7, "New #7" ],
                [8,"New #8" ]
            ]
        });
        // the edit combo
        var comboProductNo = new Ext.form.ComboBox({
            store: store_prodcutno,
            valueField: "value",
            displayField: "text"
        });

        //vat renderer
        // the renderer. You should define it within a namespace
        var comboBoxRendererVat = function(combo) {
            return function(value) {
                if(value == null || value =='' || typeof value ==='undefined')
                    return '';
                var idx = combo.store.find(combo.valueField, value);
                var rec = combo.store.getAt(idx);
                return (rec === null ? '' : rec.get(combo.displayField) + "%" );
            };
        }
        // the combo store
        var store_vat= new Ext.data.SimpleStore({
            fields: [ "value","text" ],
            data: [
                [13, "13" ],
                [12,"12"] ,
                [5, "5" ],
                [8,"8" ]
            ]
        });
        // the edit combo
        var combovat = new Ext.form.ComboBox({
            store: store_vat,
            valueField: "value",
            displayField: "text"
        });


        Ext.apply(this, {
            store: new Ext.data.TreeStore({
                model: Dag.model.Treegrid,
                proxy: {
                    type: 'ajax',
                    url: 'resources/data/treegrid-data.json'
                },
                autoSync: true,
                autoload: true,
                folderSort: true,
                listeners: {
                    datachanged: function(store, eOpts){
                       // console.log('xxxxx');
                       // console.log(store);
                    },
                    update: function(st, rec,op, modFldNames)
                    {
                        if(modFldNames == null ) return;
                        //items...
                        if(modFldNames.indexOf('Items') != -1)
                        {
                          //  console.log(rec);
                         }
                    }
                }
            }),
            columns: [{
                xtype: 'treecolumn', //this is so we know which column will show the tree
                text: 'Header',
                flex: 2,
                sortable: true,
                dataIndex: 'HeaderText',
            },
            {
                text: 'Prodcut NO',
                flex: 1,
                dataIndex: 'ProductNo',
                sortable: true,
                editor: comboProductNo, renderer: comboBoxRendererProdcutNo(comboProductNo)
            },
            {
                text: 'Items',
                flex: 1,
                dataIndex: 'Items',
                sortable: true,
                //editor
                editor : {
                    xtype :'textfield'
                }
            },
            {
                text: 'Price Per Item',
                flex: 1,
                dataIndex: 'PricePerItem',
                sortable: true,
                xtype:'templatecolumn',
                tpl:Ext.create('Ext.XTemplate','{[this.formatPrice(values)]}',{
                    formatPrice: function(v)
                    {
                        var modelType = v.modelType;
                        if(modelType =='chunk' || modelType =='section')
                            return '';
                        if( !isNaN(v.PricePerItem))
                            return '$' + v.PricePerItem.toFixed(2);
                        else
                            return '';
                    }
                }),
                //editor
                editor : {
                    xtype :'textfield'
                }
            },
              {
                //we must use the templateheader component so we can use a custom tpl
                xtype: 'templatecolumn',
                text: 'VAT CODE',
                flex: 1,
                sortable: true,
                dataIndex: 'vatCode',
                editor: combovat, renderer: comboBoxRendererVat(combovat),
                //add in the custom tpl for the rows
                tpl: Ext.create('Ext.XTemplate', '{[this.formatVatCode(values)]}', {
                    formatVatCode: function(v) {
                        var modelType = v.modelType;
                        if(modelType =='chunk' || modelType =='section')
                            return '';
                        return v.vatCode + ' %';
                    }
                })
            },
            {
                //we must use the templateheader component so we can use a custom tpl
                xtype: 'templatecolumn',
                text: 'SUM',
                flex: 1,
                sortable: true,
                align: 'center',
                //add in the custom tpl for the rows
                tpl: Ext.create('Ext.XTemplate', '{[this.formatVAT(values)]}', {
                    formatVAT: function(v) {
                        var sum = v.Sum;
                        return '$'+ sum.toFixed(2);
                    }
                })
            },
            {
                //we must use the templateheader component so we can use a custom tpl
                xtype: 'templatecolumn',
                text: 'VAT',
                flex: 1,
                sortable: true,
                align: 'center',
                //add in the custom tpl for the rows
                tpl: Ext.create('Ext.XTemplate', '{[this.formatVAT(values.Vat)]}', {
                    formatVAT: function(v) {

                        if(!isNaN(v) )
                        {
                            return '$' + v.toFixed(2);
                        }
                        else {
                            return '';
                        }
                    }
                })
            },
            {
                text: 'Flag',
                xtype: 'templatecolumn',
                width: 55,
                align: 'center',
                tpl:Ext.create('Ext.XTemplate','{[this.formatFlag(values)]}',{
                    formatFlag:function(v)
                    {
                        var ok = '<span class="glyphicon glyphicon-check"></span>';
                        var check ='<span class="glyphicon glyphicon-remove"></span>';
                        var checked = '<span class="glyphicon glyphicon-ok-circle"></span>';
                       // console.log(v);
                        //first get model Type
                        var modelType = v.modelType;

                        if(v.Flag=='ok')
                            return ok;
                        else if(v.Flag =='check')
                            return check;
                        else if(v.Flag =='checked')
                            return checked;
                        else
                            return ok;
                    }
                })
            },
            {
                    text: 'Check Button',
                    width: 55,
                    menuDisabled: true,
                    xtype: 'actioncolumn',
                    tooltip: 'Change Check',
                    align: 'center',
                    icon: 'resources/images/edit_task.png',
                    handler: function(grid, rowIndex, colIndex, actionItem, event, record, row) {

                        var modelType = record.get('modelType');
                        //check model Type
                        if(modelType=='invoice') //invoice
                        {
                            record.set({Flag:'checked'});
                        }else if(modelType =='chunk')
                        {
                            //set child invoice flag all 'checked'
                            record.set({Flag:'checked'});
                            var children = record.childNodes;
                            for(var i=0; i<children.length; i++)
                                children[i].set({Flag: 'checked'});

                        }else if(modelType =='section')
                        {
                            //set child invoice flag all 'checked'
                            record.set({Flag:'checked'});

                            var children = record.childNodes;
                            for(var i=0; i<children.length; i++)
                                children[i].set({Flag: 'checked'});
                        }

                    },
                    // Only leaf level tasks may be edited
                    isDisabled: function(view, rowIdx, colIdx, item, record) {

                        return !(record.get('Flag') =='check') & (record.get('Flag')!='');
                    }
                }
            ],
            selType : 'cellmodel',
            plugins : [
               Ext.create('Ext.grid.plugin.CellEditing',{
                    clicksToEdit:2,
                    listeners: {
                        'beforeedit':function(editor,e)
                        {
                            var record = e.record;
                            //disable editing for chunk and sections...
                            if(record.data && (record.data.modelType =='chunk' || record.data.modelType=='section'))
                                return false;
                            //for invoice it's possible
                            return true;
                        },
                        'edit': function(editor, e)
                        {
                            //grid
                            var grid =e.grid;
                            //store
                            var store = grid.store;
                            //current record
                            var record = e.record;

                            var parentId = record.data.parentId;
                            var parent = store.getById(parentId);
                            console.log('parent');
                            console.log(parent);
                            console.log('current');
                            console.log(record.data);

                            if(parent.data.modelType !='invoice' && parent.data.modelType !='')
                            {

                                //parent update -vat and sum
                                var sum = 0;
                                var vat = 0;

                                for(var i =0; i<parent.childNodes.length;i++)
                                {
                                    var childNode = parent.childNodes[i];
                                    var childData = childNode.data;
                                    sum += childData.Items * childData.PricePerItem;
                                    vat += childData.Items * childData.PricePerItem * childData.vatCode / 100;
                                }
                                parent.data.Sum = sum;
                                parent.data.Vat = vat;

                                //items
                                if(parent.data.modelType=='chunk')
                                {
                                    var rootId = parent.data.parentId;
                                    var root = store.getById(rootId);
                                    //update sum, vat, items
                                    var items= 0 ;
                                    sum = 0;
                                    vat = 0;
                                    for(var i =0; i<root.childNodes.length;i++)
                                    {
                                        var childNode = root.childNodes[i];
                                        for(var j=0; j<childNode.childNodes.length;j++)
                                        {
                                            var childData = childNode.childNodes[j].data;
                                            sum += childData.Items * childData.PricePerItem;
                                            vat += childData.Items * childData.PricePerItem * childData.vatCode / 100;
                                            items += eval(childData.Items);
                                        }

                                    }
                                    root.data.Sum = sum;
                                    root.data.Vat = vat;
                                    root.data.Items = items;
                                    root.setDirty(true)
                                }

                                parent.setDirty(true);

                               // grid.store = store;
                               grid.view.refresh();
                            }

                        }
                    }
                })
            ]
        });
        this.callParent(arguments);
    }
});
