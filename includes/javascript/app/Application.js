Ext.define('Dag.Application', {
    name: 'Dag',

    extend: 'Ext.app.Application',

    views: [
        // TODO: add views here
        'Dag.view.Main'
    ],

    controllers: [
        // TODO: add controllers here
        'Dag.controller.Main'
    ],

    stores: [
        // TODO: add stores here
    ]
});
